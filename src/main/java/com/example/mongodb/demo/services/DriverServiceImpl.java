package com.example.mongodb.demo.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.mongodb.demo.dto.DriverDTO;
import com.example.mongodb.demo.entities.Driver;
import com.example.mongodb.demo.interfaces.DriverService;
import com.example.mongodb.demo.repositories.DriverRepository;

@Service
public class DriverServiceImpl implements DriverService {

	@Autowired
	private DriverRepository repository;

	@Override
	public DriverDTO getDriver(Integer id) {
		Optional<Driver> optionalDriver = this.repository.findById(id);

		if (optionalDriver.isPresent()) {
			Driver entity = optionalDriver.get();
			DriverDTO response = this.getDriverDTO(entity);
			return response;
		}
		return null;
	}

	private DriverDTO getDriverDTO(Driver entity) {

		DriverDTO response = new DriverDTO();

		// MaGas
		response.setId(entity.getId());
		response.setName(entity.getName());

		return response;
	}

	@Override
	public Boolean addDriver(DriverDTO dto) {
		Driver entity = this.getDriverEntity(dto);
		this.repository.insert(entity);
		return true;
	}

	private Driver getDriverEntity(DriverDTO dto) {
		Driver response = new Driver();
		// MaGas
		response.setId(dto.getId());
		response.setName(dto.getName());
		return response;
	}

}
