package com.example.mongodb.demo.interfaces;

import com.example.mongodb.demo.dto.DriverDTO;

public interface DriverService {

	DriverDTO getDriver(Integer id);

	Boolean addDriver(DriverDTO dto);

}
