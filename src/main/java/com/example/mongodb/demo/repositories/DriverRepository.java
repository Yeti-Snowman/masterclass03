package com.example.mongodb.demo.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.mongodb.demo.entities.Driver;

public interface DriverRepository extends MongoRepository<Driver, Integer> {
	public Optional<Driver> findById(Integer id);

	public List<Driver> findByName(String name);
}