package com.example.mongodb.demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.mongodb.demo.dto.DriverDTO;
import com.example.mongodb.demo.interfaces.DriverService;


@RestController
@RequestMapping("/driver")
public class DriverController {

	@Autowired
	private DriverService driverService;

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<DriverDTO> getDriver(@PathVariable("id") Integer id) {
		DriverDTO response = this.driverService.getDriver(id);
		return new ResponseEntity<DriverDTO>(response, HttpStatus.OK);
	}
	
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Boolean> addAgreement(@RequestBody DriverDTO dto)  {
		Boolean result = this.driverService.addDriver(dto);
		return new ResponseEntity<Boolean>(result, HttpStatus.CREATED);
	}
	
}
